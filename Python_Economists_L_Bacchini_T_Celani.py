# -*- coding: utf-8 -*-
"""
Created on Sat Oct 15 16:46:01 2022

@author: Leonardo Bacchini, Tommaso Celani
"""
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
from statsmodels.tsa.arima.model import ARIMA
from statsmodels.graphics.tsaplots import plot_acf
import statsmodels.api as sm
import statsmodels.formula.api as smf
from sklearn.metrics import mean_squared_error, mean_absolute_percentage_error
from math import sqrt
from tabulate import tabulate
from statsmodels.tsa.stattools import adfuller
import statsmodels

pd.set_option('display.max_rows', 500)

GDP=pd.read_excel('C:/Users/tomma/Desktop/Cartella Bella/MSc Economics/Python for Econometrics\GDP_data.xls')

GDP=GDP.rename(columns = {'Italy': 'gdp'})

# Creating a new index in the form of Quarters with a time format comprehensible by Python

a = pd.Series(pd.date_range('1995', 
                       periods = 110, freq = 'Q'))

GDP['quarters'] = a

GDP.set_index('quarters', inplace=True)

GDP = GDP.drop(columns=['GEO/TIME'])

#Once the data for GDP is dealt with, we focus on the data for the ESI, our chosen indicator
ESI = pd.read_excel('C:/Users/tomma/Desktop/Cartella Bella/MSc Economics/Python for Econometrics/ESI Pulito.xlsx' )

ESI.set_index('Period', inplace=True)

#Dealing with Missing Values, using the backfill methodology
ESI.isna()
ESI.fillna(method='bfill', inplace = True)

#The ESI is a monthly dataset, here we make some modifications to obtain "quarterly" data
ESI = np.array(ESI)
ESI.shape

#Reshaping it as a 151 * 3 matrix gives us a way to transform it into quarters, obtained as averages across months
ESI2 = np.reshape(ESI, (151 , 3) )
A = np.array([[1/3], [1/3], [1/3]])
ESIQ = ESI2 @ A

#Here we do some cleaning to obtain the same period of interest as in the GDP data, dropping unused observations
ESIQ = pd.DataFrame(ESIQ)
ESIQ.shape

#It contains more observations than the ones we need
ESIQ.drop(ESIQ.index[0:40], inplace=True)
ESIQ = ESIQ.reset_index()

#We have new and old index, rename the old one and drop it.
ESIQ= ESIQ.rename(columns = {'index': 'numbers'})
ESIQ = ESIQ.drop(columns=['numbers'])

#Creation of a time series organized by Quarters, then used as an index, renaming values to ESIQuarter
sr = pd.Series(pd.date_range('1995', 
                       periods = 110, freq = 'Q'))

ESIQ['quarters1'] = sr

ESIQ = ESIQ.rename(columns = { 0 : 'ESIQuarter'})

ESIQ.set_index('quarters1', inplace=True)

ESIQ=ESIQ[:110]

#Trasform the log(ESI) into a difference data series and attach to the GDP dataset
ESIQ['ESIchange'] = sm.tsa.statespace.tools.diff(np.log(ESIQ['ESIQuarter']), 
                                     k_diff=1, k_seasonal_diff=None, seasonal_periods=1)

GDP['ESIgrowth'] = ESIQ['ESIchange']

#Some plots to see how the series behaves, and some of its properties
graphESI1=ESIQ['ESIchange'].plot()
plt.xlabel('quarters1')
plt.ylabel('ESIchange')
plt.show()

plt.acorr(ESIQ['ESIchange'].dropna(), maxlags = 10)
plt.grid(True)
plt.show()

plot_acf(ESIQ['ESIchange'].dropna(),alpha=0.1)
plt.show()

#Here we plot the GDP time series to make some preliminary analysis, including transformations as log and diff
graph1=GDP['gdp'].plot()
plt.xlabel('Time')
plt.ylabel('GDP (mln euro)')
plt.show()


GDP['log gdp']=np.log(GDP['gdp'])

graph2=GDP['log gdp'].plot()
plt.xlabel('Time')
plt.ylabel('log GDP')
plt.show()

adfuller(GDP['log gdp'], maxlag=8, regression='c',
                                   autolag='AIC', store=False, regresults=False)

GDP['gdpgrowth'] = sm.tsa.statespace.tools.diff(np.log(GDP['gdp']), 
                                     k_diff=1, k_seasonal_diff=None, seasonal_periods=1)

graph3=GDP['gdpgrowth'].plot()
plt.xlabel('Time')
plt.ylabel('GDP growth rate')
plt.show()

#Autocorrelation plot
plt.acorr(GDP['gdpgrowth'].dropna(), maxlags = 10)
plt.grid(True)
plt.show()

plot_acf(GDP['gdpgrowth'].dropna(),alpha=0.1)
plt.show()

#Check on whether the lag order is correct
p=1
d=0
q=0
mod = ARIMA(GDP['gdpgrowth'], order=(p, d, q))
res = mod.fit()
res.summary()

#Check to see whether everything is correct
print(GDP.head(10))

#The observation 81 refers to 2015Q2, our cutoff point for training set
print(GDP.iloc[81]) 

#Prediction of the ARIMA model of lag = 1, 81 is the threshold for the period of interest
est=np.zeros(109-81,dtype=float)

for t in range(82,110):
    mod = ARIMA(GDP['gdpgrowth'][t-81:t-1], order=(1, 0, 0))
    res = mod.fit()
    est[t-82]=res.params[0]+res.params[1]*GDP['gdpgrowth'][t-1]

#We bundle the estimates with the period they refer to
est=pd.DataFrame(est)
b = pd.Series(pd.date_range('2015-07-01', 
                       periods = 28, freq = 'Q'))
est['quarters']=b
est.set_index('quarters', inplace=True)
est=est.rename(columns = {0: 'AR(1)'})


#Here instead we make the prediction using regression on ESI, attaching the estimates to the AR(1)
est2 = np.zeros(109+1-82, dtype = float)

for t in range(82,110):
    model = smf.ols('gdpgrowth ~ ESIgrowth', data=GDP[:][t-81:t-1])
    res1=model.fit()
    est2[t-82] = res1.params[0]+res1.params[1]*GDP['ESIgrowth'][t]
    
est['ESI']=est2

#Here we make some graphs to check how our predictions perform against the actual GDP growth
graph4=GDP['gdpgrowth']['2015-09-30':'2022-06-30'].plot()
graph5=est['AR(1)'].plot()
graph6=est['ESI'].plot()
plt.legend(['GDP growth', 'AR(1) forecast', 'ESI forecast'])
plt.show()

graph7=GDP['gdpgrowth']['2015-09-30':'2019-09-30'].plot()
graph8=est['AR(1)'][:'2019-09-30'].plot()
graph9=est['ESI'][:'2019-09-30'].plot()
plt.legend(['GDP growth', 'AR(1) forecast', 'ESI forecast'])
plt.show()

graph10=GDP['gdpgrowth']['2019-12-31':].plot()
graph11=est['AR(1)']['2019-12-31':].plot()
graph12=est['ESI']['2019-12-31':].plot()
plt.legend(['GDP growth', 'AR(1) forecast', 'ESI forecast'])
plt.show()


#With the graph in mind, we use some statistical measures to check the goodness of fit of our model
rmse_ar = sqrt(mean_squared_error(GDP['gdpgrowth'][82:110], est['AR(1)']))
rmse_esi = sqrt(mean_squared_error(GDP['gdpgrowth'][82:110], est['ESI']))
mape_ar=mean_absolute_percentage_error(GDP['gdpgrowth'][82:110], est['AR(1)'])*100
mape_esi=mean_absolute_percentage_error(GDP['gdpgrowth'][82:110], est['ESI'])*100
print([rmse_ar,rmse_esi])
print([mape_ar,mape_esi])

Tabella = [["RMSE AR", round(rmse_ar,4)], 
        ["RMSE ESI", round(rmse_esi,4)], 
        ["MAPE AR", round(mape_ar,1)], 
        ["MAPE ESI", round(mape_esi,1)]]

colonne = ["Test", "Results"]

print(tabulate(Tabella, headers=colonne))

Tabella1 = pd.DataFrame(Tabella)

print(Tabella1.to_latex(index=True))