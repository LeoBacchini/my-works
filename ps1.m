clear all
clc
tic

%7
T=[10 20 40 80 160 1000];
M=10000;
lambda=1;
a=[-0.8 0 0.8];
t_st=NaN(length(T)*length(a),M);
z=0;
for j=a
    for t=T
        z=z+1;
        disp(t)
        w=NaN(t,1);
        x=zeros(t,1);
        W=NaN(t,M);
        X=NaN(t,M);
        for k=1:M
            rng(t*k);
            eps=normrnd(0,lambda,[t,1]);
            for i=1:t
                w(i)=j*x(i)+eps(i);
                if i<t
                    x(i+1)=w(i);
                end
            end
            Sxx=x'*x;
            Swx=w'*x;
            a_hat=Swx/Sxx;
            eps_hat=w-a_hat*x;
            lambda_hat=eps_hat'*eps_hat;
            sa_2=(1/t)*lambda_hat/Sxx;
            t_st(z,k)=(a_hat-j)/sqrt(sa_2);
        end
    end
end



%8
q=[0.005 0.01 0.025 0.05 0.1 0.25 0.5 0.75 0.9 0.95 0.975 0.99 0.995];
Q=NaN(length(q),size(t_st,1));
for i=1:size(t_st,1)
    Q(:,i)=quantile(t_st(i,:),q);
end
disp(Q)

%9
tab=array2table(Q, ...
    'VariableNames',{'a=-0.8, T=10','a=-0.8, T=20','a=-0.8, T=40','a=-0.8, T=80','a=-0.8, T=160','a=-0.8, T=1000','a=0, T=10','a=0, T=20','a=0, T=40','a=0, T=80','a=0, T=160','a=0, T=1000','a=0.8, T=10','a=0.8, T=20','a=0.8, T=40','a=0.8, T=80','a=0.8, T=160','a=0.8, T=1000'},...
    'RowNames',{'0.005' '0.01' '0.025' '0.05' '0.1' '0.25' '0.5' '0.75' '0.9' '0.95' '0.975' '0.99' '0.995'});

disp(tab)

%a=-0.8
x=-5:0.05:5;

T_10=t_st(1,:)';
Pdf_10=fitdist(T_10, 'Kernel','Kernel','epanechnikov');
y_10=pdf(Pdf_10,x);

T_160=t_st(5,:)';
Pdf_160=fitdist(T_160, 'Kernel','Kernel','epanechnikov');
y_160=pdf(Pdf_160,x);

T_1000=t_st(6,:)';
Pdf_1000=fitdist(T_1000, 'Kernel','Kernel','epanechnikov');
y_1000=pdf(Pdf_1000,x);

y_norm=normpdf(x,0,1);   

plot(x,y_10,'--r',x,y_160,'--m', x,y_1000,'--b');
hold on;
plot(x,y_norm,'-k','LineWidth',2)
title('Comparison between fit for an AR(1) with a=-0.8 with different sample sizes and a Standard Normal')
xlabel('Quantiles')
ylabel('PDF')
legend('T=10','T=160','T=1000','Standard Normal','Location','northwest')
hold off;

clear T_10 Pdf_10 y_10 T_160 Pdf_160 y_160 T_1000 Pdf_1000 y_1000

%a=0
T_10=t_st(7,:)';
Pdf_10=fitdist(T_10, 'Kernel','Kernel','epanechnikov');
y_10=pdf(Pdf_10,x);

T_160=t_st(11,:)';
Pdf_160=fitdist(T_160, 'Kernel','Kernel','epanechnikov');
y_160=pdf(Pdf_160,x);

T_1000=t_st(12,:)';
Pdf_1000=fitdist(T_1000, 'Kernel','Kernel','epanechnikov');
y_1000=pdf(Pdf_1000,x);

y_norm=normpdf(x,0,1);   

plot(x,y_10,'--r',x,y_160,'--m', x,y_1000,'--b');
hold on;
plot(x,y_norm,'-k','LineWidth',2)
title('Comparison between fit for an AR(1) with a=0 with different sample sizes and a Standard Normal')
xlabel('Quantiles')
ylabel('PDF')
legend('T=10','T=160','T=1000','Standard Normal','Location','northwest')
hold off;


clear T_10 Pdf_10 y_10 T_160 Pdf_160 y_160 T_1000 Pdf_1000 y_1000

%a=0.8
T_10=t_st(13,:)';
Pdf_10=fitdist(T_10, 'Kernel','Kernel','epanechnikov');
y_10=pdf(Pdf_10,x);

T_160=t_st(15,:)';
Pdf_160=fitdist(T_160, 'Kernel','Kernel','epanechnikov');
y_160=pdf(Pdf_160,x);

T_1000=t_st(16,:)';
Pdf_1000=fitdist(T_1000, 'Kernel','Kernel','epanechnikov');
y_1000=pdf(Pdf_1000,x);

y_norm=normpdf(x,0,1);   

plot(x,y_10,'--r',x,y_160,'--m', x,y_1000,'--b');
hold on;
plot(x,y_norm,'-k','LineWidth',2)
title('Comparison between fit for an AR(1) with a=0.8 with different sample sizes and a Standard Normal')
xlabel('Quantiles')
ylabel('PDF')
legend('T=10','T=160','T=1000','Standard Normal','Location','northwest')
hold off;


%10
P=norminv(q,0,1);
Q2=[Q P'];
tab2=array2table(Q2, ...
    'VariableNames',{'a=-0.8, T=10','a=-0.8, T=20','a=-0.8, T=40','a=-0.8, T=80','a=-0.8, T=160','a=-0.8, T=1000','a=0, T=10','a=0, T=20','a=0, T=40','a=0, T=80','a=0, T=160','a=0, T=1000','a=0.8, T=10','a=0.8, T=20','a=0.8, T=40','a=0.8, T=80','a=0.8, T=160','a=0.8, T=1000','T=inf'},...
    'RowNames',{'0.005' '0.01' '0.025' '0.05' '0.1' '0.25' '0.5' '0.75' '0.9' '0.95' '0.975' '0.99' '0.995'});

disp(tab2)


%12
z_12=0;
a_12=1;
t_st_12=NaN(length(T)*length(a_12),M);
rng('default');
for t=T
    z_12=z_12+1;
    disp(t)
    w=NaN(t,1);
    x=zeros(t,1);
    W=NaN(t,M);
    X=NaN(t,M);
    for k=1:M
        eps=normrnd(0,lambda,[t,1]);
        for i=1:t
            w(i)=a_12*x(i)+eps(i);
            if i<t
                x(i+1)=w(i);
            end
        end
        Sxx=x'*x;
        Swx=w'*x;
        a_hat=Swx/Sxx;
        eps_hat=w-a_hat*x;
        lambda_hat=eps_hat'*eps_hat;
        sa_2=(1/t)*lambda_hat/Sxx;
        t_st_12(z_12,k)=(a_hat-a_12)/sqrt(sa_2);
    end
end

disp(t_st_12)
        


Q_12=NaN(length(q),size(t_st_12,1));
for i=1:size(t_st_12,1)
    Q_12(:,i)=quantile(t_st_12(i,:),q);
end
disp(Q_12)


tab_12=array2table(Q_12, ...
    'VariableNames',{'T=10','T=20','T=40','T=80','T=160','T=1000'},...
    'RowNames',{'0.005' '0.01' '0.025' '0.05' '0.1' '0.25' '0.5' '0.75' '0.9' '0.95' '0.975' '0.99' '0.995'});

disp(tab_12)

normal=t_st_12(6,:)';
Pdf=fitdist(normal, 'Kernel','Kernel','epanechnikov');
x=-5:0.05:5;
y=pdf(Pdf,x);
y_norm=normpdf(x,0,1);   

plot(x,y,'-r',x,y_norm,'-k','LineWidth',2)
title('Comparison between fit for an AR(1) with a=1 and T=1000 sample size and a Standard Normal')
xlabel('Quantiles')
ylabel('PDF')
legend('Random walk with T=1000','Standard Normal','Location','northwest')

toc