# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 15:04:03 2019

@author: Leonardo
"""

def inverti(stringa):
    if not isinstance(stringa,str):
        raise TypeError
    inversa=''
    for i in range(len(stringa)-1,-1,-1):
        inversa+=stringa[i]
    return inversa


def cesare(stringa,n):
    if not isinstance(stringa,str):
        raise TypeError
    if not isinstance(n,int):
        raise TypeError
    alfabeto='abcdefghijklmnopqrstuvwxyz'
    tot=len(alfabeto)
    s=''
    for i in range(len(stringa)):
        if stringa[i].lower() in alfabeto:
            j=0
            while stringa[i].lower()!=alfabeto[j]:
                j+=1
            if stringa[i] in alfabeto.upper():
                alfabeto=alfabeto.upper()
            if n>=0:
                s+=alfabeto[-tot+j+n]
            else:
                s+=alfabeto[j+n]
            alfabeto=alfabeto.lower() 
        else:
            s+=stringa[i]
    return s
 


def isbooleansquare(M):
    if not isinstance(M, list):
        return False
    s=0
    if M==[]:
        return False
    n=len(M[0])
    for l in M:
        if not isinstance(l,list):
            raise TypeError
        for i in l:
            if not isinstance(i,bool):
                return False
        if len(l)!=n:
            return False
        s+=1
    if s!=n:
        return False
    return True
        

def isbooleanqueen(M):
    if isbooleansquare(M) is True:
        riga=-1
        lista=[]
        for l in M:
            riga+=1
            regine_per_riga=0
            for colonna in range(len(l)):
                if l[colonna] is True:
                    regine_per_riga+=1
                    if regine_per_riga==2:
                        return False
            
                    if colonna in lista:
                        return False
                    lista.append((riga,colonna))
            
                    counter=1
                    m=min(riga,colonna)
                    while m>=counter:
                        diagonale=(riga-counter,colonna-counter)
                        if diagonale in lista:
                            return False
                        counter+=1
                    while riga>=counter or len(l)-colonna>counter:
                        antidiagonale=(riga-counter,colonna+counter)
                        if antidiagonale in lista:
                            return False
                        counter+=1
                        
    else:
        raise ValueError
            
    return True   

    
def isqueen(text):
    if not isinstance(text,str):
        raise TypeError
        
    text=text.split()
    lista=[]
    for riga in text:
        l=[]
        for i in riga:
            if i=='.':
                l.append(False)
            elif i=='X':
                l.append(True)
            else:
                raise ValueError
        lista.append(l)
    return isbooleanqueen(lista)             


def conteggiotesto(testo,N):
    if not isinstance(testo,str):
        raise TypeError

    testo=testo.lower()
    stringa_pulita=''
    for carattere in testo:
        if carattere.isalpha():  
            stringa_pulita+=carattere
        else:
            stringa_pulita+=' ' 
    
    stringa_pulita=stringa_pulita.split()        
    
    l=[]
    for parola in stringa_pulita:
        if len(parola)==N:
            if parola not in l:
                l.append(parola)
    
    return len(l)            



def conteggiofile(nome_file,N,encoding):
    with open(nome_file,encoding=encoding) as file:
        text=file.read()
        return conteggiotesto(text,N)
        

import sqlite3
def query1(N):
    if not isinstance(N,int):
        raise TypeError
    lista_fabbriche=[]
    conn=sqlite3.connect('registro_automobilistico_db.sqlite')
    comando='select Nome_Fabbrica, sum(Numero_Versioni) from Modelli natural join Fabbriche group by Nome_Fabbrica having sum(Numero_Versioni)={};'.format(N)
    for fabbrica,versioni in conn.execute(comando):
        lista_fabbriche.append((fabbrica,))
    return lista_fabbriche        

def query2():
    lista_proprietari=[]
    conn=sqlite3.connect('registro_automobilistico_db.sqlite')
    comando='select Cognome, Nome, min(Data_Acquisto) from Proprietari natural join Proprietà group by Cod_Proprietario order by Data_Acquisto asc, Cognome asc, Nome asc;'
    for cognome,nome,data in conn.execute(comando):
        lista_proprietari.append((cognome,nome,data))
    return lista_proprietari


def query3(a,b):
    if not isinstance((a or b),int):
        raise TypeError
    l=[]
    conn=sqlite3.connect('chinook_db.sqlite')
    comando='select artists.Name,count(TrackId) from artists natural join albums join tracks on tracks.AlbumId=albums.AlbumId group by artists.ArtistId having count(TrackId) between {} and {} order by count(TrackId) desc, artists.Name asc;'.format(a,b)
    if a<=b:
        for nome,ntracce in conn.execute(comando):
            l.append((nome,ntracce))
            
    return l

    