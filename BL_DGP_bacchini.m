
T=100; 													% number of observations
N=100; 													% Number of variables
r=2; 													% number of factors
q=2;  													% number of shocks
tau=.1;													% cross correlation idiosyncratic component
mu=.6;													% max eigenvalue VAR factors
delta=.3;												% autocorrelation idiosyncratic component
theta=1;												% signal to noise ratio
p=1;													% number of lags in the VAR
iter=50; 												% number of iterations EM
tresh=10^(-4);											% threshold for stopping rule EM 
s01=1;													% s01== 1 --> standardize the data prior to estimation



%%% ========================= %%%
%%%  Simulate common factors  %%%
%%% ========================= %%%
rng(10);
u=randn(T+10,q);                                                        	% common shocks 
uu=ML_Standardize(u);                                                       % standardize common shocks 
            % ------------------------------------------------------------- % VAR Common Factors
rng(5);
A=diag(ML_uniform(q,.5,.8));                                                % diagonal elements of A(L)
A(~eye(q))=ML_uniform(q*(q-1),.1,.4);                                       % extra diagonal elements of A(L)
mut=max(eig(A));% Max Root of the polynomial
A=mu*A/(mut);                                                               % Fix max eigenvalue
            % ------------------------------------------------------------- % 
ff0=uu; for tt=2:T+10; ff0(tt,:)=(A*ff0(tt-1,:)')'+uu(tt,:); end            % Generate common factors
ff1=ML_lag(ff0,(r/q)-1,0);                                                  % take lags of the common factors
ff1=ff1(end-T+1:end,:);                                                     % keeps only the last T observations 


%%% =================================== %%%
%%%  Simulate idiosyncratic components  %%%
%%% =================================== %%%
if delta==0     % --------------------------------------------------------- % Autocorrelation idiosyncratic component
    d=zeros(N,1);                                                           % d = 0
else; d=ML_uniform(N,delta,1-2*delta);                                      % d ~ U(delta,1-2*delta)
end             % --------------------------------------------------------- %
TT1=diag(rand(N,1)+0.5); 
% variance idiosyncratic shocks
if tau==0; TT2=eye(N); else; TT2=toeplitz(tau.^(0:N-1)); end                % covariance matrix idiosyncratic shocks
TT2=TT2-triu(TT2,11)-tril(TT2,-11);                                         % keep only 10 correlations
v=randn(T+10,N)*sqrt(TT1)*cholcov(TT2);                                 	% idiosyncratic shocks 
e=v; for ii=1:N; e(:,ii)=filter(1,[1 -d(ii)],v(:,ii)); end                  % Idiosyncratic component
e(1:10,:)=[];                                                               % eliminate the first 10 observations
e=ML_center(e);                                                             % center idiosyncratic component


%%% ================================= %%%
%%%  Common components and variables  %%%
%%% ================================= %%%
LL1=1+randn(N,r);                                                           % factor loadings
chi=ML_center( ff1*LL1(1:N,:)' );                                           % common components
% ------------------------------------------------------------------------- % fix signal to noise ratio
temp=var(chi)./var(e);                                                      % ratio of variances
theta2=theta+ML_center(ML_uniform(N,theta-.25,theta+.25));                  % draw signal to noise ratio
for ii=1:N; chi(:,ii)=(1/sqrt(theta2(ii)))*chi(:,ii)/sqrt(temp(ii)); end    % impose signal to noise ratio
% ------------------------------------------------------------------------- %
x=chi+e;                                                                    % variables


%%% =============================== %%%
%%%  Identify factors and loadings  %%%
%%% =============================== %%%
[W,M] = eigs(cov(chi), r,'LM'); W=W*diag(sign(W(1,:))');                    % eigenvalue-eigenvectors of chi      
lambda=W*sqrt(M);                                                          	% identified factor loadings
f=chi*W/sqrt(M);                                                           	% identified common factors
H1=NaN(r+1,r); for rr=1:r;  H1(:,rr)=ML_ols(f(:,rr),ff1,1); end             % Rotation matrix
const=H1(1,:); H1=H1(2:end,:); H2=eye(r)/H1;                                % ---------------
AA=H1'*[A zeros(q,r-q);eye(r-q) zeros(r-q,r-q)]*H2';
G=H1'*[eye(q); zeros(r-q,r-q)];    
const2=((eye(r)-AA)*const')';

%%% ==================== %%%
%%%  Saves coefficients  %%%
%%% ==================== %%%
DGP.x=x;
DGP.lambda=lambda;
DGP.f=f;
DGP.chi=chi;
DGP.xi=e;
DGP.G=G;
DGP.A=AA;
DGP.const=const2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ML_Standardize - Standardize Variables
%
% [y M s] = ML_Standardize(x)
%
% Written by Matteo Luciani (matteoluciani@yahoo.it)

function [y, M, s] = ML_Standardize(x)
T=size(x,1);
s = nanstd(x);
M = nanmean(x);
ss = ones(T,1)*s;
MM = ones(T,1)*M;
y = (x-MM)./ss;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ML_uniform - Generate U(a,b) of size T
% x=ML_uniform(T,a,b)
% 
% Written by Matteo Luciani (matteo.luciani@ulb.ac.be)

function x=ML_uniform(T,a,b)
rng(5)
x=a+(b-a).*rand(T,1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ML_center - Demean variables
% CENTER XC = center(X)
%	Centers each column of X.
%	J. Rodrigues 26/IV/97, jrodrig@ulb.ac.be
function XC = ML_center(X)
T = size(X,1);
XC = X - ones(T,1)*(sum(X)/T); % Much faster than MEAN with a FOR loop
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ML_lag - Given x(t) produces x(t-j0) .... x(t-k)
%
% xx=ML_lag(x,k,j0)
% by default j0 is 1
%
% eg x=[x1 x2]
% xx=ML_lag(x,2)=[x1_{t-1} x1_{t-2} x2_{t-1} x2_{t-2}]
%
% Matteo Luciani (matteoluciani@yahoo.it)

function xx=ML_lag(x,k,j0)
[T, N] = size(x);

if nargin<3; j0=1; end;    
n=1;

if N*(k+1-j0)==0
    xx=[];
elseif T==1
    xx=x;
else
    xx=zeros(T-k,N*(k+1-j0));
    for i=1:N
        for j=j0:k
            xx(:,n)=x(k+1-j:T-j,i);
            n=n+1;
        end;
    end;
end;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ML_ols - OLS Estimation
% 
% [beta,u,v,esu,r2,espqr]=ML_ols(y,x,det);
%   Inputs:
%       y   = Endogenous Variable (vector)
%       x   = Exogenous Variables (matrix)
%       det = 0 no constant
%       det = 1 constant
%       det = 2 time trend
%       det = 3 constant + time trend
%   Outputs:
%       beta  = estimated coefficient
%       u     = residuals
%       v     = varcov matrix of the estimates
%       esu   = Residual Variance
%       r2    = R-Squared
%       espqr = estimates standard errors
%
% Written by Matteo Luciani (matteoluciani@yahoo.it)
% This is a modified version of the codes available on Fabio Canova webpage

function [beta,u,v,esu,r2,espar,yhat]=ML_ols(y,x,det)
T = size(x,1);

cons=ones(T,1); trend=(1:1:T)';
if      det==1; x=[cons x];
elseif  det==2; x=[trend x];
elseif  det==3; x=[cons trend x];
end;
k=size(x,2);                                                                % number of parameters
xx=eye(k)/(x'*x);                                                           % inv(x'x)
beta=xx*x'*y;                                                               % ols coeff
yhat=x*beta;                                                                % fitted values
u=y-yhat;                                                                   % residuals
uu=u'*u;                                                                    % SSR
esu=uu/(T-k);                                                               % Residual Variance
yc=y-mean(y);                                                               % Centered variables
r2=1-(uu)/(yc'*yc);                                                         % R2
v=esu*xx;                                                                   % varcov matrix of the estimates
espar=sqrt(diag(v));                                                        % Standard error
end

function DGP=BL_DGP(T,N,r,q,tau,mu,delta,theta)


%%% ========================= %%%
%%%  Simulate common factors  %%%
%%% ========================= %%%
u=randn(T+10,q);                                                        	% common shocks 
uu=ML_Standardize(u);                                                       % standardize common shocks 
            % ------------------------------------------------------------- % VAR Common Factors
A=diag(ML_uniform(q,.5,.8));                                                % diagonal elements of A(L)
A(~eye(q))=ML_uniform(q*(q-1),.1,.4);                                       % extra diagonal elements of A(L)
mut=max(eig(A));                                                            % Max Root of the polynomial
A=mu*A/(mut);                                                               % Fix max eigenvalue
            % ------------------------------------------------------------- % 
ff0=uu; for tt=2:T+10; ff0(tt,:)=(A*ff0(tt-1,:)')'+uu(tt,:); end            % Generate common factors
ff1=ML_lag(ff0,(r/q)-1,0);                                                  % take lags of the common factors
ff1=ff1(end-T+1:end,:);                                                     % keeps only the last T observations 


%%% =================================== %%%
%%%  Simulate idiosyncratic components  %%%
%%% =================================== %%%
if delta==0     % --------------------------------------------------------- % Autocorrelation idiosyncratic component
    d=zeros(N,1);                                                           % d = 0
else; d=ML_uniform(N,delta,1-2*delta);                                      % d ~ U(delta,1-2*delta)
end             % --------------------------------------------------------- %
TT1=diag(rand(N,1)+0.5);                                                    % variance idiosyncratic shocks
if tau==0; TT2=eye(N); else; TT2=toeplitz(tau.^(0:N-1)); end                % covariance matrix idiosyncratic shocks
TT2=TT2-triu(TT2,11)-tril(TT2,-11);                                         % keep only 10 correlations
v=randn(T+10,N)*sqrt(TT1)*cholcov(TT2);                                 	% idiosyncratic shocks 
e=v; for ii=1:N; e(:,ii)=filter(1,[1 -d(ii)],v(:,ii)); end                  % Idiosyncratic component
e(1:10,:)=[];                                                               % eliminate the first 10 observations
e=ML_center(e);                                                             % center idiosyncratic component


%%% ================================= %%%
%%%  Common components and variables  %%%
%%% ================================= %%%
LL1=1+randn(N,r);                                                           % factor loadings
chi=ML_center( ff1*LL1(1:N,:)' );                                           % common components
% ------------------------------------------------------------------------- % fix signal to noise ratio
temp=var(chi)./var(e);                                                      % ratio of variances
theta2=theta+ML_center(ML_uniform(N,theta-.25,theta+.25));                  % draw signal to noise ratio
for ii=1:N; chi(:,ii)=(1/sqrt(theta2(ii)))*chi(:,ii)/sqrt(temp(ii)); end    % impose signal to noise ratio
% ------------------------------------------------------------------------- %
x=chi+e;                                                                    % variables


%%% =============================== %%%
%%%  Identify factors and loadings  %%%
%%% =============================== %%%
[W,M] = eigs(cov(chi), r,'LM'); W=W*diag(sign(W(1,:))');                    % eigenvalue-eigenvectors of chi      
lambda=W*sqrt(M);                                                          	% identified factor loadings
f=chi*W/sqrt(M);                                                           	% identified common factors
H1=NaN(r+1,r); for rr=1:r;  H1(:,rr)=ML_ols(f(:,rr),ff1,1); end             % Rotation matrix
const=H1(1,:); H1=H1(2:end,:); H2=eye(r)/H1;                                % ---------------
AA=H1'*[A zeros(q,r-q);eye(r-q) zeros(r-q,r-q)]*H2';
G=H1'*[eye(q); zeros(r-q,r-q)];    
const2=((eye(r)-AA)*const')';

%%% ==================== %%%
%%%  Saves coefficients  %%%
%%% ==================== %%%
DGP.x=x;
DGP.lambda=lambda;
DGP.f=f;
DGP.chi=chi;
DGP.xi=e;
DGP.G=G;
DGP.A=AA;
DGP.const=const2;
end


