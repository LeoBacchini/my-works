global mainDIR "C:\Users\Bacchini\leonardo\unibo\causal inference\esame"
clear
matrix drop _all

set more off
cd "$mainDIR"
cap log close

set seed 10

*ssc install synth, replace
*net install synth_runner, from(https://raw.github.com/bquistorff/synth_runner/master/) replace

set more off
import excel dati_2_Vico, firstrow clear
encode State, gen(n_State)
drop State
tsset n_State Year

correlate Motordeaths Motordeaths2 Vehicles GDP PopDensity Roadusage Poisoningdeaths

tssmooth ma Motordeaths_ma = Motordeaths, window(3)
tssmooth ma Motordeaths2_ma = Motordeaths2, window(3)
tssmooth ma Vehicles_ma = Vehicles, window(3)
tssmooth ma GDP_ma = GDP, window(3)
tssmooth ma PopDensity_ma = PopDensity, window(3)
tssmooth ma Roadusage_ma = Roadusage, window(3)
tssmooth ma Poisoningdeaths_ma = Poisoningdeaths, window(3)

*foreach var of varlist Motordeaths Vehicles GDP PopDensity Roadusage Poisoningdeaths {
*    tssmooth ma3_'substr("`var'",1,1) substr("`var'",2,.)': ma = `var', window(3)
*}

*generate Motordeaths_log = ln(Motordeaths)

drop if Year == 1984
drop if Year == 2006

foreach var of varlist Motordeaths_ma Motordeaths2_ma Vehicles_ma GDP_ma PopDensity_ma Roadusage_ma Poisoningdeaths_ma {
    egen `var'_std = std(`var')
}


*synth Motordeaths_ma_std Vehicles_ma_std GDP_ma_std PopDensity_ma_std Roadusage_ma_std Motordeaths_ma_std(1994) Motordeaths_ma_std(1997),/*
**/ trunit(3) trperiod(1992) xperiod(1993(1)2002)  mspeperiod(1993(1)2002) nested fig

egen mean_covar=mean(Motordeaths2_ma_std) if 

synth Motordeaths2_ma_std Vehicles_ma_std GDP_ma_std PopDensity_ma_std Roadusage_ma_std Motordeaths2_ma_std(1994) Motordeaths2_ma_std(1997),/*
*/ trunit(3) trperiod(1993) xperiod(1993(1)2002)  mspeperiod(1993(1)2002) keep (data/synth.dta) replace fig;
graph save "figures/synth", replace

*timer clear 1
*timer on 1
*display "$S_TIME  $S_DATE"

*resultsperiod(numlist)

* Plot the gap in predicted error
use data/synth.dta, clear
keep _Y_treated _Y_synthetic _time
drop if _time==.
rename _time year
rename _Y_treated  treat
rename _Y_synthetic counterfact
gen gap=treat-counterfact
sort year
twoway (line gap year,lp(solid)lw(vthin)lcolor(black)), yline(0, lpattern(shortdash) lcolor(black)) xline(1993, lpattern(shortdash) lcolor(black)) xtitle("",si(medsmall)) xlabel(#10) ytitle("Gap in Arkansas teenage deaths prediction error", size(medsmall)) legend(off)
graph save "figures/gap", replace
save data/synth_gap_93.dta, replace 

* Table of Weights
use data/synth.dta, clear
keep _W_Weight _Co_Number
rename _W_Weight weights
rename _Co_Number name
drop if weights==0
log using tables/weights.tex, replace
list name weights, noobs
log close
esttab using tables/weights.tex, replace booktabs

**** Inference Placebo test ****
import excel dati_2_Vico, firstrow clear
encode State, gen(n_State)
drop State
tsset n_State Year

tssmooth ma Motordeaths_ma = Motordeaths, window(3)
tssmooth ma Motordeaths2_ma = Motordeaths2, window(3)
tssmooth ma Vehicles_ma = Vehicles, window(3)
tssmooth ma GDP_ma = GDP, window(3)
tssmooth ma PopDensity_ma = PopDensity, window(3)
tssmooth ma Roadusage_ma = Roadusage, window(3)
tssmooth ma Poisoningdeaths_ma = Poisoningdeaths, window(3)

drop if Year == 1984
drop if Year == 2006

foreach var of varlist Motordeaths_ma Motordeaths2_ma Vehicles_ma GDP_ma PopDensity_ma Roadusage_ma Poisoningdeaths_ma {
    egen `var'_std = std(`var')
}


forval i=1/24{
	synth  Motordeaths2_ma_std Vehicles_ma_std GDP_ma_std PopDensity_ma_std Roadusage_ma_std Motordeaths2_ma_std(1994) Motordeaths2_ma_std(1997), trunit(`i') trperiod(1993) xperiod(1993(1)2002) mspeperiod(1993(1)2002) keep(data/synth_`i', replace)
matrix state`i' = e(RMSPE)
}

forval i=1/24 { 
matrix rownames state`i'=`i' 
matlist state`i', names(rows) 
}

**** Compute post and pre-RMSPE ****

 forval i=1/24 {
    use data/synth_`i' ,clear
    keep _Y_treated _Y_synthetic _time
    drop if _time==.
    rename _time year
    rename _Y_treated  treat`i'
    rename _Y_synthetic counterfact`i'
    gen gap`i'=treat`i'-counterfact`i'
    sort year 
    save data/synth_gap_`i', replace
}
use data/synth_gap_93.dta, clear
sort year
save data/placebo93.dta, replace

forval i=1/24 {
	merge year using data/synth_gap_`i' 
	drop _merge 
	sort year 
    save data/placebo.dta, replace 
}

**** Estimate the pre- and post-RMSPE and calculate the ratio of the post-pre RMSPE ****

forval i=1/24 {
	use data/synth_gap_`i', clear
    gen gap93=gap`i'*gap`i'
    egen postmean=mean(gap93) if year>=1993
    egen premean=mean(gap93) if year<1993
    gen rmspe=sqrt(premean) if year<1993
    replace rmspe=sqrt(postmean) if year>=1993
    gen ratio=rmspe/rmspe[_n+1] if 1992
    gen rmspe_post=rmspe[_n+1] if 1992
    gen rmspe_pre=sqrt(premean) if year<1993
    mkmat rmspe_pre rmspe_post ratio if 1992, matrix (state`i')
}

 forval i=1/24 {
 	matrix rownames state`i'=`i'
	matrix state`i' = state`i'[5..5,.]
	matlist state`i', names(rows)
}

mat state=state1\state2\state3\state4\state5\state6\state7\state8\state9\state10\state11\state12\state13\state14\state15\state16\state17\state18\state19\state20\state21\state22\state23\state24

*ssc install mat2txt
**** Distribution of ratios of post-RMSPE to pre-RMSPE ****
mat2txt, matrix(state) saving(data/rmspe.txt) replace
    insheet using data/rmspe.txt, clear
    ren v1 state
    drop v5
    gsort -ratio
    gen rank=_n
    gen p=rank/25
    export excel using data/rmspe, firstrow(variables) replace
    import excel data/rmspe.xls, sheet("Sheet1") firstrow clear
    histogram ratio, bin(20) frequency fcolor(gs13) lcolor(black) ylabel(0(2)6) xtitle(Post/pre RMSPE ratio) xlabel(0(0.5)3)
graph save "figures/histo", replace

list rank p if state==3

**** Placebo distribution: all the placeboes on the same picture ****
use data/placebo.dta, replace
twoway 
(line gap1 year ,lp(solid)lw(vthin)) 
(line gap2 year ,lp(solid)lw(vthin))
(line gap3 year, lp(solid)lw(thick)lcolor(black)) /*treatment unit, Arkansas*/
(line gap4 year ,lp(solid)lw(vthin)) 
(line gap5 year ,lp(solid)lw(vthin))
(line gap6 year ,lp(solid)lw(vthin))
(line gap7 year ,lp(solid)lw(vthin)) 
(line gap8 year ,lp(solid)lw(vthin)) 
(line gap9 year ,lp(solid)lw(vthin)) 
(line gap10 year ,lp(solid)lw(vthin)) 
(line gap11 year ,lp(solid)lw(vthin)) 
(line gap12 year ,lp(solid)lw(vthin)) 
(line gap13 year ,lp(solid)lw(vthin))
(line gap14 year ,lp(solid)lw(vthin)) 
(line gap15 year ,lp(solid)lw(vthin)) 
(line gap16 year ,lp(solid)lw(vthin)) 
(line gap17 year ,lp(solid)lw(vthin))
(line gap18 year ,lp(solid)lw(vthin))
(line gap19 year ,lp(solid)lw(vthin)) 
(line gap20 year ,lp(solid)lw(vthin)) 
(line gap21 year ,lp(solid)lw(vthin)) 
(line gap22 year ,lp(solid)lw(vthin)) 
(line gap23 year ,lp(solid)lw(vthin)) 
(line gap24 year ,lp(solid)lw(vthin)), 
yline(0, lpattern(shortdash) lcolor(black)) xline(1993, lpattern(shortdash) lcolor(black)) xtitle("",si(small)) xlabel(#10) ytitle("Placebo distribution", size(small))
    legend(off);
	graph save "figures/placebo", replace
