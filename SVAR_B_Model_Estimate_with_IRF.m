% SVAR
clear 
clc
close all
clear global     % cleans all gloabl variables

% addpath ..\

%% Global Variables 
global p             % # of VAR lags
global T             % # of observations in dataset   
global Sigma_u       % error covariance matrix
global ParamNumber   % needs to select the structural parameters to estimate in the structural form

%% Data set
load DB_Uncertainty.txt;   % it loads dataset in txt format

% Variables (of interest in dataset)
% 1 UF1	   US financial uncertainty measure 1-month ahead
% 2 UF3	   US financial uncertainty measure 3-month ahead      
% 3 UF12   US financial uncertainty measure 12-month ahead
% // financial uncertainty measures taken
% from Ludvigson, Ma and Ng (2109, American Economic Journal:
% Macroeconomics) //
%
% 4 UM1	   US macroeconomic uncertainty measure 1-month ahead
% 5 UM3	   US macroecconomic uncertainty measure 3-month ahead
% 6 UM12   US macroeconomic uncertainty measure 12-month ahead	
% // macroeconomic uncertainty measures taken
% from Jurado, Ludivigson and Ng (2105, American Economic Review) //
%
% 7 IP	   US industrial production index
% 8 emp	   US employment (loggged)
% 9 lip	   US industrial production index logged
% 10 lemp  US employment (loggged)
	
% 11 Dlip	growth rate of US industrial production (not in % terms) 
% 12 Dlemp	growth rate of US employment (not in % terms)
% 13 Dprod  growth rate of US industrial production (in % terms)

% Sample 
% 1 1960M8
% end 2015M4 (T=657)

VariablesSelected = [1 4 13];  % selects UF1 (1), UM1 (4) and Dprod (13), respectively
StartSample = 1; % this starts from first observation, i.e. 1960M8
EndSample = 657;

DataSet=DB_Uncertainty(StartSample:EndSample,VariablesSelected);


%% DataSet
p = 5;                    % # of lags for the estimated VAR
T = size(DataSet,1)-p;   
M = size(DataSet,2);      % VAR dimension M  

% Duplication Matrix     % this builds the Duplication matrix to be used
% next
DuplicationMatrix = DuplicationMatrixFunction(M);
mDD=(DuplicationMatrix'*DuplicationMatrix)^(-1)*DuplicationMatrix';  % this is the D_plus matrix in the slides
mNN=DuplicationMatrix*mDD;


W = DataSet(p+1:EndSample,:);   % W matrix of data in slides
 
X=[];                 % matrix of regressors X - empty for no

for i = 1 : p        % loop that fills with data the matrix of regressors X
Reg = DataSet(p+1-i:EndSample-i,:);
X = [X Reg];
end

manual_const = ones(size(W,1),1);

X = [X manual_const];  % the constant is added in the matrix of regressors at the end

%% VAR Estimate

VAR_Const = [NaN; NaN; NaN];  % VAR contant, Mx1 vector; note thay
                              % if you put zero in place of NaN, that will be managed
                              % as a zero constraint in estimation
 
 Pi1 =[NaN NaN NaN;     % Pi matrices collecting parametters associated with lags. The same as before if you put zero you constraint
       NaN NaN NaN; 
       NaN NaN NaN];
 Pi2 =[NaN NaN NaN; 
       NaN NaN NaN; 
       NaN NaN NaN];      % recall that if you put zero in place of NaN, that will be managed
                          % as a zero constraint in estimation
 Pi3 =[NaN NaN NaN; 
       NaN NaN NaN; 
       NaN NaN NaN];
 Pi4 =[NaN NaN NaN; 
       NaN NaN NaN; 
       NaN NaN NaN]; 
 Pi5 =[NaN NaN NaN; 
       NaN NaN NaN; 
       NaN NaN NaN];
 %Pi6 =[NaN NaN NaN; 
 %      NaN NaN NaN; 
 %      NaN NaN NaN];

 VAR_Pi = {Pi1 Pi2 Pi3 Pi4 Pi5};  % autoregressive parameters, Pi matrix in slides
         
       
% Mdl = varm('Constant',VAR_Const,'AR',VAR_Pi, 'Trend',[NaN; NaN; NaN; 0;%
% 0]); % linear trend FORGET ABOUT THIS FOR THE MOMENT

VAR = varm('Constant',VAR_Const,'AR',VAR_Pi);             % Matlab command for VAR specification feeded with constant and Pi parameters
[EstVAR,EstSE,logLikVAR,Residuals] = estimate(VAR,DataSet);  % Estimation by ML, the "estimate()" command is provided by Matlab

Const = EstVAR.Constant        % Here we put the estimates in the left-hand-side matrices            
mP1 = EstVAR.AR{1,1}
mP2 = EstVAR.AR{1,2}
mP3 = EstVAR.AR{1,3}
mP4 = EstVAR.AR{1,4}
mP5 = EstVAR.AR{1,5}
%mP6 = EstVAR.AR{1,6}

% OLS estimator with matrix calculations

Pi_ols = (W'*X)*(X'*X)^(-1);   % hand_made OLS estimator of autoregressive parameters

U = W - X*Pi_ols';            % hand-made residuals

Estimated_Autoregressive=Pi_ols';

Estimated_Autoregressive 

logLikVAR % returns the log-likelihood of unrestricted reduced form VAR

% EstMdl.Covariance
Sigma_u = (Residuals'*Residuals)/T  % estimated covariance matrix could also be (U'U)/T
Sigma_u_Sample = Sigma_u;
%% Cholesky factor

CChol = chol(Sigma_u)'

%% SVAR estimation (B-model) with ML

Sigma_u_sample = Sigma_u;
          
ParamNumber =[1 2 ...     % first column of B, notice that element (3,1) is set to zero
              5 6 ...     % second column of B, notice that position (1,2) corresponding to 4 is set to zero
              9]';        % third column of B, positions (1,3) and (2,3) corresponding to 7 and 8 are set to zero 
          
% size(ParamNumber,1)
Matrix_Selected = zeros(size(Sigma_u_sample,1),size(Sigma_u_sample,1));

for c_par = 1 : size(ParamNumber,1)
Matrix_Selected(ParamNumber(c_par,1)) = 1;    % this loop puts parameters in matrix form
end

StructuralParam = size(ParamNumber,1);        % dimension of vector of structural parameters (beta in the slides)

InitialValue_B=(randn(StructuralParam,1)/10);  % initial random values for B in order to enter the likelihood maximization

% Likelihood Maximization
options = optimset('MaxFunEvals',200000,'TolFun',1e-500,'MaxIter',200000,'TolX',1e-50);   
[StructuralParam_Estimation_MATRIX,Likelihood_SVAR,exitflag,output,grad,Hessian_MATRIX] = fminunc('Likelihood_SVAR',InitialValue_B',options);

SE_Hessian_MATRIX = diag(inv(Hessian_MATRIX)).^0.5;  % computes Hessian-based standard errors

B = zeros(size(Sigma_u,1),size(Sigma_u,1));
SE_B = zeros(size(Sigma_u,1),size(Sigma_u,1));
HSelection = zeros(M*M,StructuralParam);       % this matrix corresponds to selection matrix S_B in slides 

for c_par = 1 : size(ParamNumber,1)
B(ParamNumber(c_par,1)) = StructuralParam_Estimation_MATRIX(c_par);     % puts the estimated elements of B in the right place  
SE_B(ParamNumber(c_par,1))= SE_Hessian_MATRIX(c_par);
HSelection(ParamNumber(c_par,1),c_par) = 1;                            % puts "1" in the correct place of selection matrix S_B 
end

% Sign normalization  (recall that identification holds up to sign normalization)   
    if B(1,1)<0
    B(:,1)=-B(:,1);
    end
    if B(2,2)<0
    B(:,2)=-B(:,2); 
    end
    if B(3,3)<0
    B(:,3)=-B(:,3);
    end

B    

SE_B 
 
Likelihood_SVAR = -1*Likelihood_SVAR   % returns the log-likelihood of SVAR

LR_test_overid = -2*(Likelihood_SVAR - logLikVAR)


%% Rank conditions    
VRank=2*kron(B,eye(M));
RankMatrix=kron(eye(1),mDD)*[VRank]; 
Jacobian= RankMatrix*HSelection;
rank(Jacobian)
size(ParamNumber,1)

%% IRFs


HorizonIRF = 20;

C_IRF = B;     % instantaneous impact at h=0

J=[eye(M) zeros(M,M*(p-1))];                          % selection matrix J used in IRF computation 

CompanionMatrix = [mP1 mP2 mP3 mP4 mP5;           % VAR companion matrix
                   eye(M*(p-1)) zeros(M*(p-1),M)];
                       
    for h = 0 : HorizonIRF
    TETA(:,:,h+1)=J*CompanionMatrix^h*J'*C_IRF;
    end
    
TETA    








