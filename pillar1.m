clc, clear
cd('C:/Users/Bacchini/leonardo/cresme')

%import DB_prov_new_1
DBprovnew = readtable('DB.xlsx');

D=DBprovnew(:,[9:18,66,75]);

%% Costruzione database indici
DB_ind.Eco1=table2array(D(:,1));
DB_ind.Eco2=table2array(D(:,2));
DB_ind.Eco3=table2array(D(:,3));
DB_ind.Eco4=table2array(D(:,4));
DB_ind.Eco5=table2array(D(:,5));
DB_ind.Eco6=table2array(D(:,6));
DB_ind.Eco7=table2array(D(:,7));
DB_ind.Eco8=table2array(D(:,8));
DB_ind.Eco9=table2array(D(:,9));
DB_ind.Eco10=table2array(D(:,10));
DB_ind.Eco11=table2array(D(:,11));
DB_ind.Eco12=table2array(D(:,12));




%% Correlation matrix (finding subpillars)
[rho,pval]=corr(D.Variables,'type','Pearson');
labels=D.Properties.VariableNames;
f=figure;
f.WindowState='fullscreen';
subplot(2,1,1)
h=heatmap(labels,labels,rho);
h.FontSize=9;
subplot(2,1,2)
h=heatmap(labels,labels,pval);
h.FontSize=9;
saveas(f,'1_MCorr_Eco.png','png')
close 'all'


%% Index definition (subpillars)

Xi={[DB_ind.Eco2 DB_ind.Eco1 DB_ind.Eco3 DB_ind.Eco12],... % ricchezza pro-capite
    [DB_ind.Eco4 DB_ind.Eco5 DB_ind.Eco10],... % crescita
    [DB_ind.Eco6 DB_ind.Eco7],... % tessuto imprenditoriale
    [DB_ind.Eco8 DB_ind.Eco9],... % dinamismo imprese
    [DB_ind.Eco11]}; % pensioni anzianità
Score=cell(0);
for k=1:size(Xi,2)
    I=Xi{k}./repmat(std(Xi{k}),size(Xi{k},1),1); % standardizing
    vord=1; % the ordering variable (always the first one)
    [P,Is,v]=pca(I);
    if P(vord,1)>0, T=Is; sig=+1; else T=-Is; sig=-1; end
    figure
    hh=heatmap(P);
    minT=repmat(min(T),size(T,1),1);
    T=T-minT; p=[v/sum(v)]';
    W=repmat(p,size(T,1),1);
    U=(prod((T+100).^W,2)).^10;
    Score{k}=U/max(U)*100; %  the score variable
end

%% Final score
N=size(D,1);
FScore=zeros(N,1);
w=[0.2,0.2,0.2,0.2,0.2]; % subpillar weights (set by hand)
for l=1:size(Score,2)
   FScore=FScore+w(l)*Score{l};
end
FScore=FScore/abs(max(FScore))*100;
%
PScore=zeros(size(FScore));
for n=1:size(PScore,1)
   y=FScore;
   PScore(n)=numel(y(y<FScore(n)))/numel(y); 
end

%% resulting correlation analysis
X=table2array(D);
tbll=array2table([X,FScore],'VariableNames',[labels,'Score']);
f=figure;
corrplot(tbll)
f.WindowState='fullscreen';
saveas(f,'1_FCorr_Eco.png','png')
close 'all'
%%
Output=X; Xprog=X(:,1); L=size(Output,2);
for j=1:size(Output,1)
   if not(isempty(FScore(Xprog==Output(j,1))))  
       Output(j,L+1)=FScore(Xprog==Output(j,1));
       Output(j,L+2)=PScore(Xprog==Output(j,1));
   else
       Output(j,L+1)=NaN;
       Output(j,L+2)=NaN;
   end
end
%%
Olabel=[labels,'(Eco) Score','(Eco) Percentile'];
Output=[Olabel;num2cell(Output)];
%%