clear all
set more off
capture log close
log using "ps1_group13", text replace


/*QUESTION 1*/


/*1*/
quietly set obs 100
set seed 1013

matrix mu=(10,15,15,10)
matrix sigma=(1,0.6,0,0.2\0.6,1,0,0.3\0,0,1,0\0.2,0.3,0,1)

drawnorm y x1 x2 x3, cov(sigma) means(mu)

describe y x1 x2 x3
summarize y x1 x2 x3

gen cons=1

/*1a*/

mata
sigmax=(1,0,0.3\0,1,0\0.3,0,1)
sigmaxy=(0.6\0\0.2)
muy=10
mux=(15,15,10)
beta=invsym(sigmax)*sigmaxy
b0=muy-mux*beta
b0
b1=beta[1,1]
b2=beta[2,1]
b3=beta[3,1]
b1
b2
b3
end

/*1b su pdf*/

/*1c*/
mata
st_view(Y=.,.,"y")
st_view(X=.,.,("cons","x1", "x2", "x3"))
beta = invsym(X'X)*X'Y
beta
end


/*1d*/
mata
st_view(Y=.,.,"y")
st_view(X=.,.,("cons","x1", "x2", "x3"))
beta = invsym(X'X)*X'Y
sst = Y'Y-rows(Y)*mean(Y)^2 
sst
sse = beta'X'Y-rows(Y)*mean(Y)^2
sse
ssr = Y'Y-beta'X'Y
ssr
end


/*1e*/
mata
st_view(Y=.,.,"y")
st_view(X=.,.,("cons","x1", "x2", "x3"))
beta = invsym(X'X)*X'Y
n = rows(X)
k = cols(X)-1
sst = Y'Y-rows(Y)*mean(Y)^2 
sse = beta'X'Y-rows(Y)*mean(Y)^2
R2=sse/sst
R2
R2_adj=1-(1-R2)*(n-1)/(n-k-1)
R2_adj
end

reg y x1 x2 x3

/*1f*/
mata
st_view(Y=.,.,"y")
st_view(X=.,.,("cons","x1", "x2", "x3"))
beta = invsym(X'X)*X'Y
u_hat=Y-X*beta
u_hat
Y_hat=X*beta
Y_hat
end

/*1g*/
mata
st_view(Y=.,.,"y")
st_view(X=.,.,("cons","x1", "x2", "x3"))
beta = invsym(X'X)*X'Y
u_hat=Y-X*beta
n = rows(X)
av_u_hat=(1/n)*(sum(u_hat))
av_u_hat
cov_x_u_hat=X'*u_hat
cov_x_u_hat
end

/*pdf*/

/*1h*/
mata
st_view(Y=.,.,"y")
st_view(X=.,.,("cons","x1", "x2", "x3"))
beta = invsym(X'X)*X'Y
beta
Y_hat=X*beta
av_Y=mean(Y)
av_Y_hat=mean(Y_hat)
av_Y
av_Y_hat
end

/*pdf*/




/*2*/
reg y x1 x2 x3
/*pdf*/



/*3a*/
capture program drop random_sample 

program define random_sample, rclass
	drop _all
	scalar drop _all
	matrix drop _all
	set more off
	set obs 100
	matrix mu=(10,15,15,10)
    matrix sigma=(1,0.6,0,0.2\0.6,1,0,0.3\0,0,1,0\0.2,0.3,0,1)
    drawnorm y x1 x2 x3, cov(sigma) means(mu)
    reg y x1 x2 x3
    return scalar beta0 = _b[_cons]  
    return scalar beta1 = _b[x1]
    return scalar beta2 = _b[x2]
    return scalar beta3 = _b[x3]
end

simulate ///
beta0 = r(beta0) ///
beta1 = r(beta1) ///
beta2 = r(beta2) ///
beta3 = r(beta3), reps(1000) ///
saving(g13,replace) seed(1013): random_sample

/*3b*/
putmata B0 = (beta0), replace
putmata B1 = (beta1), replace
putmata B2 = (beta2), replace
putmata B3 = (beta3), replace

mata
sigmax=(1,0,0.3\0,1,0\0.3,0,1)
sigmaxy=(0.6\0\0.2)
muy=10
mux=(15,15,10)
beta=invsym(sigmax)*sigmaxy
b0=muy-mux*beta
b1=beta[1,1]
b2=beta[2,1]
b3=beta[3,1]
Beta = (b0\b1\b2\b3)
Beta	
BETA=(B0,B1,B2,B3)	    
EBETA = (mean(BETA))'
EBETA
bias = EBETA - Beta		    
bias
end

/*pdf*/


/*3c*/
hist beta0, normal
hist beta1, normal
hist beta2, normal
hist beta3, normal

/*pdf*/




/*QUESTION 2*/
clear all

use ps1_group13
edit
bro
describe
summarize


/*1*/

gen labor_supply=hourswm
gen cons=1

mata
st_view(Y=.,.,"labor_supply")
st_view(X2=.,.,("agem1","agefstm","blackm","hispm","othracem","educm"))
st_view(X1=.,.,"morekids")
st_view(c=.,.,"cons")
X=(c,X1,X2)
beta = invsym(X'X)*X'Y
beta0=beta[1,1]
beta1=beta[2,1]
beta2=beta[3\4\5\6\7\8,1]
beta0
beta1
beta2
end


/*2*/
reg labor_supply morekids agem1 agefstm blackm hispm othracem educm
gen b1=_b[morekids]
/*pdf*/


/*3a*/
reg labor_supply morekids educm
gen B1=_b[morekids]
/*pdf*/


/*3b*/
compare b1 B1
/*pdf*/

/*3c*/
mata
st_view(Y=.,.,"labor_supply")
st_view(X1=.,.,("cons","morekids"))
st_view(X2=.,.,"educm")
gamma = invsym(X1'X1)*X1'Y
u_hat=Y-X1*gamma
alpha=invsym(X1'X1)*X1'X2
x2_hat=X2-X1*alpha
beta2=invsym(x2_hat'x2_hat)*x2_hat'u_hat
beta2
end

reg labor_supply morekids educm

log close